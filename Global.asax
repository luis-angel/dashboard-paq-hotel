﻿<%@ Application Language="C#" %>

<script runat="server">

    

    void Application_Start(object sender, EventArgs e) 
    {
        // Código que se ejecuta al iniciarse la aplicación
        //Context.Response.Write(System.Globalization.CultureInfo.CurrentCulture.Name);

        /*
        string pais = "MX";
        instDatos da = new instDatos();
        System.Globalization.RegionInfo ResolveC;
        ArrayList infoPais = new ArrayList();

        ResolveC = ResolveCountry();
        
        if (ResolveC != null)
            pais = ResolveC.TwoLetterISORegionName;

        infoPais = da.obtenerMonedaPais(pais);

        if (infoPais.Capacity > 0)
        {
            HttpContext.Current.Session["sPais"] = infoPais[0].ToString();
            HttpContext.Current.Session["sMoneda"] = infoPais[1].ToString();
            HttpContext.Current.Session["iMoneda"] = infoPais[2];
        }*/

    }

       
    void Application_End(object sender, EventArgs e) 
    {
        //  Código que se ejecuta cuando se cierra la aplicación

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Código que se ejecuta al producirse un error no controlado

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Código que se ejecuta cuando se inicia una nueva sesión

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Código que se ejecuta cuando finaliza una sesión. 
        // Nota: El evento Session_End se desencadena sólo con el modo sessionstate
        // se establece como InProc en el archivo Web.config. Si el modo de sesión se establece como StateServer 
        // o SQLServer, el evento no se genera.

    }

    void Application_AcquireRequestState(object sender, EventArgs e)
    {

        /*
        if (HttpContext.Current.Session != null)
        {
            var len = "es";

            if (Session["len"] != null)
            {
                len = Session["len"].ToString();
            }
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(len);
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(len);
        }*/

        if (HttpContext.Current.Session != null)
        {
            //string pais = "MX";            
            //System.Globalization.RegionInfo ResolveC;
            //ArrayList infoPais = new ArrayList();

            //ResolveC = ResolveCountry();

            if (HttpContext.Current.Session["sPais"] == null)
            {

                //if (ResolveC != null)
                //    pais = ResolveC.TwoLetterISORegionName;

                /*
                infoPais = da.obtenerMonedaPais(pais);

                if (infoPais.Capacity > 0)
                {
                    HttpContext.Current.Session["sPais"] = infoPais[0];
                    HttpContext.Current.Session["sMoneda"] = infoPais[1];
                    HttpContext.Current.Session["iMoneda"] = infoPais[2];
                }
                else
                {
                    HttpContext.Current.Session["sPais"] = "MX";
                    HttpContext.Current.Session["sMoneda"] = "mxn";
                    HttpContext.Current.Session["iMoneda"] = "1";
                }*/

                HttpContext.Current.Session["sPais"] = "MX";
                HttpContext.Current.Session["sMoneda"] = "mxn";
                HttpContext.Current.Session["iMoneda"] = "1";

            }
            else
            {

                if (HttpContext.Current.Session["monedaMaestra"] != null)
                {
                   
                }

            }
        }

        
        
        
    }


    public static System.Globalization.CultureInfo ResolveCulture()
    {

        string[] languages = HttpContext.Current.Request.UserLanguages;



        if (languages == null || languages.Length == 0)

            return null;



        try
        {

            string language = languages[0].ToLowerInvariant().Trim();

            return System.Globalization.CultureInfo.CreateSpecificCulture(language);

        }

        catch (ArgumentException)
        {

            return null;

        }

    }

    public static System.Globalization.RegionInfo ResolveCountry()
    {

        System.Globalization.CultureInfo culture = ResolveCulture();

        if (culture != null)

            return new System.Globalization.RegionInfo(culture.LCID);



        return null;

    }  
       
</script>
